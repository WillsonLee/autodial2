<?php

namespace App\Http\Controllers\Telephony;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\telephony\users;
use Illuminate\Support\Facades\Config;
use App\Models\Local\DatabaseConnection;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showUsers(){

        //this is to get connection from DbConnectController class
        app('App\Http\Controllers\Settings\DbConnectController')->getDfaultDB();

        //$users = DB::connection('mysql2')->table('vicidial_users')->get();
        //$users = users::all();

        // //test set config for db to another place , scuesses
        // Config::set("database.connections.mysql2", [
        //     "driver" => "mysql",
        //     "host" => "192.168.1.7",
        //     "port" => "3306",
        //     "database" => "asterisk",
        //     "username" => "root",
        //     "password" => "vicidialnow"
        // ]);
        $users = '';
        try{
            DB::connection('mysql2')->getPdo();
            $users = users::all();
            return view('Telephony.users')->with('users',$users);
        } catch(\Exception $e) {
            //die("Error connect to remote database : " .$e);

            $dbs = DatabaseConnection::all();
            return redirect('settings/showdbs')->with('dbs',$dbs);
            //return view('sample');
            //return view('Telephony.users')->with('users',$users);
        }

        //below 3 line is set diff connection.
        //$userss = new users;
        //$userss->setConnection('mysql2');
        //$users = $userss->get();

    }

}
