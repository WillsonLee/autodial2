<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\Models\Local\DatabaseConnection;
//temp use user
use App\Models\telephony\users;
class DbConnectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showDBs(){

        //return view('Settings.showdbs');

        // Config::set("database.connections.mysql2", [
        //     "driver" => "mysql",
        //     "host" => "192.168.1.7",
        //     "port" => "3306",
        //     "database" => "asterisk",
        //     "username" => "root",
        //     "password" => "vicidialnow"
        // ]);

        //$users = users::all();

        //below 3 line is set diff connection.
        //$userss = new users;
        //$userss->setConnection('mysql2');
        //$users = $userss->get();
        //return view('Settings.showdbs')->with('users',$users);

        $dbs = DatabaseConnection::all();
        //return view('Settings.showdbs')->with('dbs',$dbs);
        return view('settings.showdbs', compact('dbs'));
    }

    public function jsupdate(Request $request){
        $DBdata = new DatabaseConnection;
        try{
            //$any = $DBdata::where('id',$request->id)->first();
            //     // find only search for id in the table
            // $any = $DBdata::find($request->id);
            // $selected = $request->selected;
            // if($selected === 1){
            //     $selected = 1;
            // }else{
            //     $selected = 0;
            // }

            $DBdata::where('selected',1)
                    ->update([
                        'selected' => 0,
                    ]);

            $DBdata::find($request->id)
                    ->update([
                            'driver' => $request->driver,
                            'host' => $request->host,
                            'port' => $request->port,
                            'database' => $request->database,
                            'username' => $request->username,
                            'password' => $request->password,
                            'selected' => $request->selected,
                    ]);

            // $dbs = DatabaseConnection::all();
            // $returnHTML = view('Settings.showdbs')->with('dbs',$dbs)->render();
            // return response()->json(array('status' => 'success', 'html'=>$returnHTML));

            return response()->json([
                'status' => 'success',
                'url' => '/settings/showdbs'
            ]);

        }catch(ModelNotFoundException $exception){
           return $exception->getMessage();
        }

        // $DBdata::where('host',$request->host)
        // ->where('database',$request->database)
        // ->where('username'$request->username)
        // ->update([
        //     'driver' => $request->driver,
        //     'host' => $request->host,
        //     'port' => $request->port,
        //     'database' => $request->database,
        //     'username' => $request->username,
        //     'password' => $request->password,
        //     'selected' => $request->selected,
        // ]);

        //return $request->all();
    }

    public function jsdelete(Request $request){

        /*
            Post::find($id); acts upon the primary key,
            if you have set your primary key in your model to something other than id by doing:
            protected  $primaryKey = 'other-primery-key';
            $post = Post::find($id);
            $post->delete();

            $post = Post::where('id', $id);
            $post->delete();
        */

        $DBdata = new DatabaseConnection;
        try{
            $DBdata::find($request->id)->delete();

            return response()->json([
                'status' => 'success',
                'url' => '/settings/showdbs'
            ]);

        }catch(ModelNotFoundException $exception){
            return response()->json([
                'status' => 'failed',
                'url' => $exception->getMessage()
            ]);
           //return $exception->getMessage();
        }

    }

    public function addDB(Request $request){
        // $response = array(
        //     'status' => 'success',
        //     'msg' => $request->message,
        // );

        //return $request->all();
        $DBdata = new DatabaseConnection;
        $DBdata->driver = $request->driver;
        $DBdata->host = $request->host;
        $DBdata->port = $request->port;
        $DBdata->database = $request->database;
        $DBdata->username = $request->username;
        $DBdata->password = $request->password;

        $DBdata->save();

        $dbs = DatabaseConnection::all();
        return redirect('settings/showdbs')->with('dbs',$dbs);
    }

    public function getDfaultDB(){
        // this is to get connection from DbConnectController class

        // this can use only if the controller is at "app\Http\Controllers"
        // app(DbConnectController::class)->setDfaultDB()

        // use this
        // app('App\Http\Controllers\Settings\DbConnectController')->getDfaultDB();

        $DBdata = new DatabaseConnection;
        $dbselect = $DBdata::where('selected',1)->first();

        Config::set("database.connections.mysql2", [
            "driver" => $dbselect->driver,
            "host" => $dbselect->host,
            "port" => $dbselect->port,
            "database" => $dbselect->database,
            "username" => $dbselect->username,
            "password" => $dbselect->password
        ]);
    }
}
