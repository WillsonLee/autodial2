<?php

namespace App\Models\telephony;

use Illuminate\Database\Eloquent\Model;

class users extends Model
{
    //set table name
    protected $table = 'vicidial_users';

    //set connection
    protected $connection = 'mysql2';
}
