<?php

namespace App\Models\Local;

use Illuminate\Database\Eloquent\Model;

class DatabaseConnection extends Model
{
    /*
     * Post::find($id); acts upon the primary key,
     * if you have set your primary key in your model to something other than id by doing:
     * protected  $primaryKey = 'other-primery-key';
     */
    protected $fillable = [
        'id','driver','host', 'port', 'database', 'username', 'password', 'selected',
    ];
}
