@extends('layouts.mainlayout')

@section('title')
    <title>Database Connection</title>

    <!-- provide the csrf token in <head>-->
    <meta name="csrf_token" content="{{ csrf_token() }}" />
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/plugins/dataTables/datatables.min.css')}}">
    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{{asset('css/plugins/sweetalert/sweetalert.css')}}">

    <style>
        #overlay {
            position: fixed;
            display: block;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.5);
            z-index: 2;
            cursor: pointer;
        }

        .centerdiv {
        position: absolute;
        top: 30%;
        left: 50%;
        font-size: 20px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
        background-color: rgba(0,0,0,0.5);
        }

    </style>
@endsection

@section('js')

    <!-- Sweet alert -->
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 10,
                responsive: true
            });

        $('#overlay').fadeOut();
        });
    </script>

    <script type="text/javascript">
        $("body").on("click", ".btn-edit", function(){
            var id = $(this).parents("tr").attr('data-id');
            var driver = $(this).parents("tr").attr('data-driver');
            var host = $(this).parents("tr").attr('data-host');
            var port = $(this).parents("tr").attr('data-port');
            var dbname = $(this).parents("tr").attr('data-dbname');
            var username = $(this).parents("tr").attr('data-username');
            var password = $(this).parents("tr").attr('data-password');
            var selected = $(this).parents("tr").attr('data-selected');

            $(this).parents("tr").find("td:eq(1)").html('<input name="edit_driver" value="'+driver+'" style="width: 100%">');
            $(this).parents("tr").find("td:eq(2)").html('<input name="edit_host" value="'+host+'" style="width: 100%">');
            $(this).parents("tr").find("td:eq(3)").html('<input name="edit_port" value="'+port+'" style="width: 100%">');
            $(this).parents("tr").find("td:eq(4)").html('<input name="edit_dbname" value="'+dbname+'" style="width: 100%">');
            $(this).parents("tr").find("td:eq(5)").html('<input name="edit_username" value="'+username+'" style="width: 100%">');
            $(this).parents("tr").find("td:eq(6)").html('<input name="edit_password" value="'+password+'" style="width: 100%">');

            if(selected == 1){
                $(this).parents("tr").find("td:eq(7)").html('<input name="edit_selected" type="checkbox" id="chk'+id+'" value="1" checked>');
            }else{
                $(this).parents("tr").find("td:eq(7)").html('<input name="edit_selected" type="checkbox" id="chk'+id+'" value="0">');
            }

            $(this).parents("tr").find("td:eq(8)").prepend("<button class='btn btn-info btn-xs btn-update'>Update</button><button class='btn btn-warning btn-xs btn-cancel'>Cancel</button>")
            $(this).hide();
        });

    </script>

    <script type="text/javascript">
        $("body").on("click", ".btn-cancel", function(){
            var driver = $(this).parents("tr").attr('data-driver');
            var host = $(this).parents("tr").attr('data-host');
            var port = $(this).parents("tr").attr('data-port');
            var dbname = $(this).parents("tr").attr('data-dbname');
            var username = $(this).parents("tr").attr('data-username');
            var password = $(this).parents("tr").attr('data-password');
            var selected = $(this).parents("tr").attr('data-selected');

            $(this).parents("tr").find("td:eq(1)").text(driver);
            $(this).parents("tr").find("td:eq(2)").text(host);
            $(this).parents("tr").find("td:eq(3)").text(port);
            $(this).parents("tr").find("td:eq(4)").text(dbname);
            $(this).parents("tr").find("td:eq(5)").text(username);
            $(this).parents("tr").find("td:eq(6)").text(password);
            if(selected == 1){
                $(this).parents("tr").find("td:eq(7)").html('<input name="edit_selected" type="checkbox" value="1" checked disabled> ');
            }else{
                $(this).parents("tr").find("td:eq(7)").html('<input name="edit_selected" type="checkbox" value="0" disabled>');
            }

            $(this).parents("tr").find(".btn-edit").show();
            $(this).parents("tr").find(".btn-update").remove();
            $(this).parents("tr").find(".btn-cancel").remove();
        });
    </script>

    <script type="text/javascript">
        $("body").on("click", ".btn-delete", function(){
            //$(this).parents("tr").remove();

            var id = $(this).parents("tr").attr('data-id');
            var CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: '/settings/showdbs/jsdelete',
                    type:"POST",
                    beforeSend: function (xhr) {
                        if (CSRF_TOKEN) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
                        }
                    },
                    data: {
                        _token: CSRF_TOKEN,
                        id: id,
                    },
                    success:function(data){
                        //alert(JSON.stringify(data));
                        if (data.status == "success"){
                            window.location.href = data.url;
                        }
                        else{
                            alert("error occured");
                        }
                    },error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("error : " + XMLHttpRequest.responseText);
                    }
                }); //end of ajax
                swal("Deleted!", "Your imaginary file has been deleted.", "success");
            });


        });
    </script>

    <script type="text/javascript">
        $("body").on("click", ".btn-update", function(){
            var id = $(this).parents("tr").attr('data-id');
            var driver = $(this).parents("tr").find("input[name='edit_driver']").val();
            var host = $(this).parents("tr").find("input[name='edit_host']").val();
            var port = $(this).parents("tr").find("input[name='edit_port']").val();
            var dbname = $(this).parents("tr").find("input[name='edit_dbname']").val();
            var username = $(this).parents("tr").find("input[name='edit_username']").val();
            var password = $(this).parents("tr").find("input[name='edit_password']").val();

            var selected = document.getElementById("chk"+id);
            var selectedval;
            // If selected is tick, log will show '1', else show 'on'
            //console.log("selected : " + selected);
            var CSRF_TOKEN = $('meta[name="csrf_token"]').attr('content');


            if(selected.checked == true){
                selectedval = 1;
            }else{
                selectedval = 0;
            }

            if(selected.checked == true){
                swal({
                    title: "Checkbox been ticked",
                    text: "set this database as default database?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#00FF00",
                    confirmButtonText: "Yes, Update it!",
                    cancelButtonText: "No, cancel pls!",
                    closeOnConfirm: false,
                    closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '/settings/showdbs/jsupdate',
                                type:"POST",
                                beforeSend: function (xhr) {
                                    if (CSRF_TOKEN) {
                                        return xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
                                    }
                                },
                                data: {
                                    _token: CSRF_TOKEN,
                                    id: id,
                                    driver: driver,
                                    host: host,
                                    port: port,
                                    database: dbname,
                                    username: username,
                                    password: password,
                                    selected: selectedval
                                },
                                success:function(data){
                                    //alert(JSON.stringify(data));
                                    if (data.status == "success"){
                                        window.location.href = data.url;

                                        //dbtable div defined on page
                                        //as this project have layout, it through all back to the div, no good
                                        //unless we have a blade file with no layout extent
                                        //$('#idname').html(data.html);
                                    }
                                    else{
                                        alert("error occured");
                                    }
                                },error: function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert("error : " + XMLHttpRequest.responseText);
                                }
                            }); //end of ajax
                            //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        } else {
                            swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });//end of swal
            }else{
                $.ajax({
                    url: '/settings/showdbs/jsupdate',
                    type:"POST",
                    beforeSend: function (xhr) {
                        if (CSRF_TOKEN) {
                            return xhr.setRequestHeader('X-CSRF-TOKEN', CSRF_TOKEN);
                        }
                    },
                    data: {
                        _token: CSRF_TOKEN,
                        id: id,
                        driver: driver,
                        host: host,
                        port: port,
                        database: dbname,
                        username: username,
                        password: password,
                        selected: selectedval
                    },
                    success:function(data){
                        //alert(JSON.stringify(data));
                        if (data.status == "success"){
                            window.location.href = data.url;

                            //dbtable div defined on page
                            //as this project have layout, it through all back to the div, no good
                            //unless we have a blade file with no layout extent
                            //$('#idname').html(data.html);
                        }
                        else{
                            alert("error occured");
                        }
                    },error: function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("error : " + XMLHttpRequest.responseText);
                    }
                }); //end of ajax
            }


            $(this).parents("tr").find("td:eq(1)").text(driver);
            $(this).parents("tr").find("td:eq(2)").text(host);
            $(this).parents("tr").find("td:eq(3)").text(port);
            $(this).parents("tr").find("td:eq(4)").text(dbname);
            $(this).parents("tr").find("td:eq(5)").text(username);
            $(this).parents("tr").find("td:eq(6)").text(password);

            if(selected == 1){
                $(this).parents("tr").find("td:eq(7)").html('<input name="edit_selected" type="checkbox" value="1" checked disabled> ');
            }else{
                $(this).parents("tr").find("td:eq(7)").html('<input name="edit_selected" type="checkbox" value="0" disabled>');
            }
            // selected is tick, los show '1', else show 'on'
            //console.log("selected : " + selected);

            $(this).parents("tr").attr('data-driver', driver);
            $(this).parents("tr").attr('data-host', host);
            $(this).parents("tr").attr('data-port', port);
            $(this).parents("tr").attr('data-dbname', dbname);
            $(this).parents("tr").attr('data-username', username);
            $(this).parents("tr").attr('data-password', password);
            $(this).parents("tr").attr('data-selected', selected);

            $(this).parents("tr").find(".btn-edit").show();
            $(this).parents("tr").find(".btn-cancel").remove();
            $(this).parents("tr").find(".btn-update").remove();
        });


    </script>
@endsection

@section('body')


    <div id="divBreadcrumb" class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>
                <span id="l_maintitle">Database Connection</span></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Settings</li><li class="breadcrumb-item active"><strong>Database Connection</strong></li>
            </ol>
        </div>
    </div>

    <div id="overlay">
        <div class="centerdiv">
        <button class="buttonload">
            <i class="fa fa-spinner fa-spin"></i>Processing...
            </button>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox border-bottom">
                <div class="ibox-title">
                    <a class="collapse-link">
                        <h5>Add Database</h5>
                    </a>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content" style="display: none">
                    <form action="/settings/showdbs/adddb" method="POST">
                        @csrf
                        <div class="form-group">
                        <label>Driver:</label>
                        <input type="text" name="driver" class="form-control" value="mysql" required="">
                        </div>

                        <div class="form-group">
                        <label>Host:</label>
                        <input type="text" name="host" class="form-control" value="" placeholder="IP Address" required="" autofocus>
                        </div>

                        <div class="form-group">
                        <label>Port:</label>
                        <input type="text" name="port" class="form-control" value="3306" required="">
                        </div>

                        <div class="form-group">
                        <label>Database Name:</label>
                        <input type="text" name="database" class="form-control" value="" placeholder="Database Name" required="">
                        </div>

                        <div class="form-group">
                        <label>Username:</label>
                        <input type="text" name="username" class="form-control" value="" placeholder="Username" required="">
                        </div>

                        <div class="form-group">
                        <label>Password:</label>
                        <input type="text" name="password" class="form-control" value="" placeholder="Password" required="">
                        </div>

                        <button type="submit" class="btn btn-success save-btn">Save</button>
                    </form>
                </div>
            </div>

            <div class="ibox ">
                <div class="ibox-content">
                    <br/>
                    <table class="table table-bordered data-table table-striped table-hover dataTables-example">
                        <thead>
                        <th>ID</th>
                        <th>Driver</th>
                        <th>Host</th>
                        <th>Port</th>
                        <th>Database Name</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Selected</th>
                        <th width="200px">Action</th>
                        </thead>
                        <tbody id="tableBody">
                            @foreach($dbs as $db)
                                <tr
                                    data-id='{{$db->id}}'
                                    data-driver='{{$db->driver}}'
                                    data-host='{{$db->host}}'
                                    data-port='{{$db->port}}'
                                    data-dbname='{{$db->database}}'
                                    data-username='{{$db->username}}'
                                    data-password='{{$db->password}}'
                                    data-selected='{{$db->selected}}'
                                >

                                <td>{{$db->id}}</td>
                                <td>{{$db->driver}}</td>
                                <td>{{$db->host}}</td>
                                <td>{{$db->port}}</td>
                                <td>{{$db->database}}</td>
                                <td>{{$db->username}}</td>
                                <td>{{$db->password}}</td>
                                <td>
                                    @if ($db->selected == '1')
                                        <input type="checkbox" value="1" checked disabled>
                                    @else
                                        <input type="checkbox" value="0" disabled>
                                    @endif
                                </td>
                                <td><button class='btn btn-info btn-xs btn-edit'>Edit</button><button class='btn btn-danger btn-xs btn-delete'>Delete</button></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
