@extends('layouts.mainlayout')

@section('title')
    <title>Dashboard</title>
@endsection

@section('css')
@endsection

@section('js')

    <!-- ChartJS-->
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <!-- <script src="js/demo/chartjs-demo.js"></script> -->

    <script>
        $(document).ready(function() {
            console.log("dashboard run!!!");
            //line chart for Call Per Hours
            var lineData = {labels: ["12AM","1AM","2AM","3AM","4AM","5AM","6AM","7AM","8AM","9AM","10AM","11AM","12PM","1PM","2PM","3PM","4PM","5PM","6PM","7PM","8PM","9PM","10PM","11PM"],
            datasets: [
                {
                    label: "Outbound",
                    backgroundColor: "rgba(26,179,148,0.7)",
                    borderColor: "rgba(26,179,148,0.7)",
                    pointBackgroundColor: "rgba(26,179,148,1)",
                    pointBorderColor: "#fff",fill: false,
                    pointRadius: 4,
                    pointHoverRadius: 7,
                    data: [0,0,0,0,0,0,0,0,0,0,334,438,390,31,416,401,314,346,109,0,0,0,0,]
                },
                {
                    label: "Inbound",
                    backgroundColor: "rgb(181, 184, 207)",
                    borderColor: "rgb(181, 184, 207)",
                    pointBackgroundColor: "rgb(112, 115, 142)",
                    pointBorderColor: "#fff",
                    fill: false,
                    pointRadius: 4,
                    pointHoverRadius: 7,
                    data: [0,1,0,0,0,0,0,0,3,0,16,16,26,8,22,23,35,23,15,1,10,1,1,1]
                },
                {
                    label: "Dropped",
                    backgroundColor: "rgb(222, 222, 222)",
                    borderColor: "rgb(222, 222, 222)",
                    pointBackgroundColor: "rgba(127, 127, 127, 0.9)",
                    pointBorderColor: "#fff",
                    fill: false, pointRadius: 4,
                    pointHoverRadius: 7,
                    data: [0,0,0,0,0,0,0,0,0,0,112,159,164,13,131,150,96,149,50,1,0,0,0,0]
                }
            ]
                };
                var lineOptions = { responsive: true };
                var ctx = document.getElementById("lineChart").getContext("2d");
                new Chart(ctx,
                    {
                        type: 'line',
                        data: lineData,
                        options: lineOptions
                    });

            //bar chart for Outbound Disposition
            var barData = {
                labels: ["Untouched","Effective","Ineffective","In Progress","Inbound"],
                datasets: [
                            {
                                label: "Project 1",
                                backgroundColor: "rgba(26,179,148,0.5)",
                                borderColor: "rgba(26,179,148,0.7)",
                                pointBackgroundColor: "rgba(26,179,148,1)",
                                pointBorderColor: "#fff",
                                data: [0,0,5,2,-1]
                            }
                    ]};

            var barOptions = { responsive: true };
            var ctx2 = document.getElementById("barChart").getContext("2d");
            new Chart(ctx2, {
                type: 'bar',
                data: barData,
                options:
                barOptions
            });

            //doughnut cahrt for Success Calls Rate
            var doughnutData = {
                labels: ["Success", "Dropped"],
                datasets: [
                    {
                        data: [40, 17],
                        backgroundColor: ["#a3e1d4", "#b5b8cf"]
                    }
                ]};
            var doughnutOptions = { responsive: true };
            var ctx4 = document.getElementById("doughnutChart").getContext("2d");
            new Chart(ctx4, {
                type: 'doughnut',
                data: doughnutData,
                options: doughnutOptions
            });


            //end of document.ready
        });


    </script>

@endsection

@section('body')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="widget style1 lazur-bg">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-phone fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> total Call </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 navy-bg">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-headphones fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> Answered Call </span>
                            <h2 class="font-bold">26'C</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 yellow-bg">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-phone-square fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> Incoming Call </span>
                            <h2 class="font-bold">260</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget style1 red-bg">
                    <div class="row">
                        <div class="col-4">
                            <i class="fa fa-microphone-slash fa-5x"></i>
                        </div>
                        <div class="col-8 text-right">
                            <span> Abandon Call </span>
                            <h2 class="font-bold">12</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Call Per Hours</h5>
                        <div class="ibox-tools">
                            <a class="fullscreen-link">
                                <i class="fa fa-expand"></i>
                            </a>

                        </div>
                    </div>
                    <div class="ibox-content">
                        <canvas id="lineChart" height="74"></canvas>
                        </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <span class="label label-success float-right">Today</span>
                        </div>
                        <h5>Sale</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">386,200</h1>
                        <div class="stat-percent font-bold text-success">98% <i class="fa fa-level-up"></i></div>
                        <small>Sales Won</small>
                    </div>
                </div>

                <div class="ibox ">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <span class="label label-info float-right">Today</span>
                        </div>
                        <h5>Online Agent</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">80,800</h1>
                        <div class="stat-percent font-bold text-info">20% <i class="fa fa-bolt"></i></div>
                        <small>Agent On Call</small>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-4">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Top 10 Agent</h5>
                        <div class="ibox-tools">
                                <a id="ContentPlaceHolder1_btnMore" href="javascript:__doPostBack(&#39;ctl00$ContentPlaceHolder1$btnMore&#39;,&#39;&#39;)"><i class="fa fa-expand" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Outbound Disposition</h5>
                        <div class="ibox-tools">
                            <a class="fullscreen-link">
                                <i class="fa fa-expand"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <canvas id="barChart" height="140"></canvas>
                            </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Success Calls Rate</h5>
                        <div class="ibox-tools">
                            <a class="fullscreen-link">
                                <i class="fa fa-expand"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                            <canvas id="doughnutChart" height="140"></canvas>
                            </div>
                </div>
            </div>

        </div>

    </div>


@endsection
