@extends('layouts.mainlayout')

@section('title')
    <title>SampleTitle</title>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/sample.css')}}">
@endsection

@section('js')
    <script src="{{ asset('js/sample.js') }}" defer></script>
@endsection

@section('body')
    <div id="divBreadcrumb" class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>
                <span id="l_maintitle">SampleTitle</span></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">MainTab</li><li class="breadcrumb-item active"><strong>SampleTitle</strong></li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <br>
                <div class="ibox-content">

                </div>
            </div>
        </div>
    </div>
@endsection
