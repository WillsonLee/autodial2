@extends('layouts.mainlayout')

@section('title')
    <title>Account</title>
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('body')
<div id="divBreadcrumb" class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>
            <span id="l_maintitle">My Profile</span></h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">Settings</li><li class="breadcrumb-item">Account</li><li class="breadcrumb-item active"><strong>My Profile</strong></li>

        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Account Information</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">


                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">User Code</label>
                        <div class="col-sm-10">
                            <span id="ContentPlaceHolder1_lblUserCode">1000160.00003</span>
                        </div>
                    </div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">User Name</label>
                        <div class="col-sm-10">
                            <span id="ContentPlaceHolder1_lblUserName">jenson</span>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Login ID</label>
                        <div class="col-sm-10">
                            <input name="ctl00$ContentPlaceHolder1$txtLoginID" type="text" value="jenson" id="ContentPlaceHolder1_txtLoginID" class="form-control" required="" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Contact Information</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <input name="ctl00$ContentPlaceHolder1$txtAddr1" type="text" id="ContentPlaceHolder1_txtAddr1" class="form-control" />
                            <br />
                            <input name="ctl00$ContentPlaceHolder1$txtAddr2" type="text" id="ContentPlaceHolder1_txtAddr2" class="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">City</label>
                        <div class="col-sm-10">
                            <input name="ctl00$ContentPlaceHolder1$txtCity" type="text" id="ContentPlaceHolder1_txtCity" class="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Post Code</label>
                        <div class="col-sm-10">
                            <input name="ctl00$ContentPlaceHolder1$txtPostcode" type="text" id="ContentPlaceHolder1_txtPostcode" class="form-control" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">State</label>
                        <div class="col-sm-10">
                            <select name="ctl00$ContentPlaceHolder1$cboState" id="ContentPlaceHolder1_cboState" class="form-control">
                                <option selected="selected" value="">-</option>
                                <option value="JHR">JOHOR</option>
                                <option value="KDH">KEDAH</option>
                                <option value="KTN">KELANTAN</option>
                                <option value="KL">KUALA LUMPUR</option>
                                <option value="LBN">LABUAN</option>
                                <option value="MLK">MELAKA</option>
                                <option value="NS">NEGERI SEMBILAN</option>
                                <option value="PHG">PAHANG</option>
                                <option value="PRK">PERAK</option>
                                <option value="PER">PERLIS</option>
                                <option value="PP">PULAU PINANG</option>
                                <option value="SBH">SABAH</option>
                                <option value="SWK">SARAWAK</option>
                                <option value="SEL">SELANGOR</option>
                                <option value="TRG">TERENGGANU</option>
                                <option value="WP">WILAYAH PERSEKUTUAN</option>
                            </select>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Handphone No</label>
                        <div class="col-sm-10">
                            <input name="ctl00$ContentPlaceHolder1$txtMobile" type="text" value="0126977130" id="ContentPlaceHolder1_txtMobile" class="form-control" required="" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Email Address</label>
                        <div class="col-sm-10">
                            <input name="ctl00$ContentPlaceHolder1$txtEmail" type="email" value="jenson.agency@gmail.com" id="ContentPlaceHolder1_txtEmail" class="form-control" required="" />
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">

                            <input type="submit" name="ctl00$ContentPlaceHolder1$btnUpdate" value="Update" id="ContentPlaceHolder1_btnUpdate" class="btn btn-primary" data-style="expand-right" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
