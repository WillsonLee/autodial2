@extends('layouts.mainlayout')

@section('title')
    <title>Users</title>
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('css/plugins/dataTables/datatables.min.css')}}">
    <!-- Sweet Alert -->
    <link rel="stylesheet" href="{{asset('css/plugins/sweetalert/sweetalert.css')}}">
@endsection

@section('js')
    <!-- Sweet alert -->
    <script src="{{ asset('js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');

                                $(win.document.body).find('table')
                                        .addClass('compact')
                                        .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        });
    </script>
@endsection

@section('body')
    <div id="divBreadcrumb" class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>
                <span id="l_maintitle">Users</span></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Telephony</li><li class="breadcrumb-item active"><strong>Users</strong></li>
            </ol>
        </div>
    </div>

    @if(count($users)>0)
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <br>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="userstable" >
                            <thead>
                            <tr>
                                <th>User ID</th>
                                <th>User Name</th>
                                <th>User Full Name</th>
                                <th>User Level</th>
                                <th>User Group</th>
                                <th>User Phone Login</th>
                            </tr>
                            </thead>

                            <tbody id="tableBody">
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->user_id}}</td>
                                        <td>{{$user->user}}</td>
                                        <td>{{$user->full_name}}</td>
                                        <td>{{$user->user_level}}</td>
                                        <td>{{$user->user_group}}</td>
                                        <td>{{$user->phone_login}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>User ID</th>
                                <th>User Name</th>
                                <th>User Full Name</th>
                                <th>User Level</th>
                                <th>User Group</th>
                                <th>User Phone Login</th>
                            </tr>
                            </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else
        <p>No Data</p>
    @endif
@endsection
