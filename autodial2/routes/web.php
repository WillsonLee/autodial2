<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
// ->name('dashboard) are use in controller when use redirect like = return redirect()->route('dashboard');

//Route::view('/editacc',"editacc");
Route::get('/profile', 'ProfileController@index');
Route::get('/changepassword','ChangePasswordController@showChangePasswordForm');
Route::post('/changePassword','ChangePasswordController@changePassword')->name('changePassword');


Route::view('/sample',"sample");

//telephony
Route::get('/telephony/users','Telephony\UsersController@showUsers');

//settings
Route::get('/settings/showdbs','Settings\DbConnectController@showDBs');
Route::post('/settings/showdbs/adddb','Settings\DbConnectController@addDB');
Route::post('/settings/showdbs/jsupdate','Settings\DbConnectController@jsupdate');
Route::post('/settings/showdbs/jsdelete','Settings\DbConnectController@jsdelete');

Route::get('/get', function(){
    //$any = DB::connection('mysql')->table('users')->get();
    //$any = DB::connection('mysql2')->table('vicidial_users')->get();
    //dump($any);

});
